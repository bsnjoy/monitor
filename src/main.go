package main

import (
//      "log"
        "os/exec"
        "net/http"
)

var playing = false;

func cillit(w http.ResponseWriter, r *http.Request) {
        if playing {
                w.Write([]byte("already playing! Wait to finish and call again!"))
        } else {
                playing = true
                w.Write([]byte("starting cillitbong video!"))
                play := exec.Command("/home/pi/monitor/play.sh", "")
                //log.Printf("Running command non blocking...")
                play.Start()
                // wait for the program to end in a goroutine
                go func() {
                        play.Wait()
                        // logic to run once process finished. Send err in channel if necessary
                        playing = false
                }()
        }
}

func main() {
        http.HandleFunc("/cillit", cillit)
        if err := http.ListenAndServe(":9999", nil); err != nil {
                panic(err)
        }
}
